//
//  SettingsScreen.swift
//  BusOnTime
//
//  Created by Otto Linden on 15/03/2019.
//  Copyright © 2019 Otto Linden. All rights reserved.
//

import UIKit

class SettingsScreen: ViewController {

    override func viewDidLoad() {
        
        let ac = UIAlertController(title: "Denied", message: "If you still want to see you location on the map, you have to go to settings...", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Okay", style: .default))
        present(ac, animated: true)
        // Do any additional setup after loading the view.
    }

    
}
