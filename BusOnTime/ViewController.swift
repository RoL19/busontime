//
//  ViewController.swift
//  BusOnTime
//
//  Created by Otto Linden on 12/03/2019.
//  Copyright © 2019 Otto Linden. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Foundation

//API STUFF
typealias Route = [RouteStuff]

struct RouteStuff: Codable {
    let routeID, agencyID, routeShortName, routeLongName: String
    let routeType: Int
    let routeColor, routeTextColor: String
    
    enum CodingKeys: String, CodingKey {
        case routeID = "route_id"
        case agencyID = "agency_id"
        case routeShortName = "route_short_name"
        case routeLongName = "route_long_name"
        case routeType = "route_type"
        case routeColor = "route_color"
        case routeTextColor = "route_text_color"
    }
}

struct FoliVm: Codable {
    let sys, status: String
    let servertime: Int
    let result: Result
}

struct Result: Codable {
    let responsetimestamp: Int
    let producerref, responsemessageidentifier: String
    let status, moredata: Bool
    let vehicles: [String: Vehicle]
}

struct Vehicle: Codable {
    let recordedattime, validuntiltime: Int
    let monitored, incongestion, inpanic: Bool
    let vehicleref: String
    let operatorref: String?
    let linkdistance: Int?
    let lineref, directionref, publishedlinename, originref: String?
    let originname, destinationref, destinationname: String?
    let originaimeddeparturetime, destinationaimedarrivaltime: Int?
    let longitude, latitude: Double?
    let delay: String?
    let delaysecs: Int?
    let blockref, nextStoppointref: String?
    let nextVisitnumber: Int?
    let nextStoppointname: String?
    let vehicleatstop: Bool?
    let nextDestinationdisplay: String?
    let nextAimedarrivaltime, nextExpectedarrivaltime, nextAimeddeparturetime, nextExpecteddeparturetime: Int?
    let onwardcalls: [Onwardcall]?
    let tripref: String?
    
    enum CodingKeys: String, CodingKey {
        case recordedattime, validuntiltime, monitored, incongestion, inpanic, vehicleref, operatorref, linkdistance, lineref, directionref, publishedlinename, originref, originname, destinationref, destinationname, originaimeddeparturetime, destinationaimedarrivaltime, longitude, latitude, delay, delaysecs, blockref
        case nextStoppointref = "next_stoppointref"
        case nextVisitnumber = "next_visitnumber"
        case nextStoppointname = "next_stoppointname"
        case vehicleatstop
        case nextDestinationdisplay = "next_destinationdisplay"
        case nextAimedarrivaltime = "next_aimedarrivaltime"
        case nextExpectedarrivaltime = "next_expectedarrivaltime"
        case nextAimeddeparturetime = "next_aimeddeparturetime"
        case nextExpecteddeparturetime = "next_expecteddeparturetime"
        case onwardcalls
        case tripref = "__tripref"
    }
}

struct Onwardcall: Codable {
    let stoppointref: String
    let visitnumber: Int
    let stoppointname: String
    let aimedarrivaltime, expectedarrivaltime, aimeddeparturetime, expecteddeparturetime: Int
}


typealias trips = [WelcomeElement]

struct WelcomeElement: Codable {
    let serviceID, tripID, tripHeadsign, routeID: String
    let directionID: Int
    let blockID, shapeID: String
    let wheelchairAccessible: Int
    
    enum CodingKeys: String, CodingKey {
        case serviceID = "service_id"
        case tripID = "trip_id"
        case tripHeadsign = "trip_headsign"
        case directionID = "direction_id"
        case blockID = "block_id"
        case shapeID = "shape_id"
        case wheelchairAccessible = "wheelchair_accessible"
        case routeID = "route_id"
    }
}


typealias latlong = [Welcome]

struct Welcome: Codable {
    let lat, lon, traveled: Double
}


class ViewController: UIViewController {
    
    // Map stuff
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 12500
    var askt = 0
    
    
    // Bus Stuff
    var busNames = [String]()
    var selectedRoute: String?
    let busPicker = UIPickerView()
    var selectedRouteId: String?
    var RouteShortName = [String]()
    var selectedTripId = [String]()
    var selectedShapeId = [String]()
    var selectedBusLat = [Double]()
    var selectedBusLong = [Double]()
    var selectedBusLineLat = [Double]()
    var selectedBusLineLong = [Double]()
    var polyLine: MKPolyline!
//    var lastSelectedShapeID = [String]()
    
    
    @IBOutlet var MapView: MKMapView!
    @IBOutlet var BusLabel: UITextField!
    @IBOutlet var MenuButton: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        setMapView()
        setBusLabel()
//        checkLocationServices()
        setUpBusAPI()
        createBusPicker()
        createToolBar()
        
        
        
        
    }
    
    @IBAction func reloadTapped(_ sender: Any) {
        print("reloadTapped")
        setUpBusAPI()
    }
    
    
    func setUpBusAPI() {
        let jsonUrlString = "https://data.foli.fi/gtfs/v0/routes"
        
        guard let url = URL(string: jsonUrlString) else { print("url error"); return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //check err
            //check response status 299 ok
            
            guard let data = data else { print("data error"); return }
            
            do {
                
                var routes = try JSONDecoder().decode(Route.self, from: data)
                
                self.busNames.removeAll()
                
                for data in routes {
                    self.busNames.append(data.routeShortName + " " + data.routeLongName)
                    
                   
                    func checkRoute() {
                        guard let route = self.selectedRoute else { return }
                        
                        

                        if route == data.routeShortName + " " + data.routeLongName {
                            self.selectedRouteId = data.routeID
    
                        } else {
//                            print(data.routeShortName + " " + data.routeLongName)
                        }
                    }
                    
                    checkRoute()
                    
                }
                
                
                
                self.busNames = self.busNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                
                if self.selectedRouteId != nil {
                    self.setupTrips()
                }
                
                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }.resume()
    }
    
    func setupTrips() {
        let jsonUrlString = "https://data.foli.fi/gtfs/v0/trips/all"
        
        guard let url = URL(string: jsonUrlString) else { print("url error"); return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //check err
            //check response status 299 ok
            
            guard let data = data else { print("data error"); return }
            
            do {
                
                let Trips = try JSONDecoder().decode(trips.self, from: data)
                
//                func lastShapeId() {
//                self.lastSelectedShapeID.removeAll()
//                    guard let shapeId = self.selectedShapeId else { print("selectedShapeId not"); return }
//                    self.lastSelectedShapeID = [shapeId]
//                }
//
//                lastShapeId()
                
                self.selectedShapeId.removeAll()
                self.selectedTripId.removeAll()
                for data in Trips {
                    if self.selectedRouteId == data.routeID {
                        self.selectedShapeId.append(data.shapeID)
                        
                        self.selectedTripId.append(data.tripID)
//                        print("data in trip: \(data.shapeID)")
                    } else {
//                        print("selected: \(self.selectedShapeId)")
//                        print("data: \(data.shapeID)")
                    }
                }
                
                
                
                self.setupVm()
//                print("called")
                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
        }.resume()
    }
    
func setupVm() {
    let jsonUrlString = "https://data.foli.fi/siri/vm"
    
    guard let url = URL(string: jsonUrlString) else { print("url error"); return }
    
    URLSession.shared.dataTask(with: url) { (data, response, err) in
        //check err
        //check response status 299 ok
        
        guard let data = data else { print("data error"); return }
        
        do {
            
            var vm = try JSONDecoder().decode(FoliVm.self, from: data)
            
            self.selectedBusLong.removeAll()
            self.selectedBusLat.removeAll()
//            print("removed")
            
            for (key, value) in vm.result.vehicles {
                
                func checkTripId() {
                        guard let tripId = value.tripref else { return }
                        if self.selectedTripId.contains(tripId){
                            self.selectedBusLat.append(value.latitude!)
                            self.selectedBusLong.append(value.longitude!)
//                            print("append")
//                            print(self.selectedBusLong)
                        } else {
                            
                        }
                    }
                
                checkTripId()
                }
//            print("done")
            self.setupShape()
        } catch let jsonErr {
            print("Error serializing json:", jsonErr)
        }
        }.resume()
}
    
    func getMostCommonShape(array: [String]) -> [String] {
//        print(selectedShapeId)
        
        var topShapes = [String]()
        var shapeDictionary: [String: Int] = [:]
    
        for shape in array {
            if let count = shapeDictionary[shape] {
                shapeDictionary[shape] = count + 1
            } else {
                shapeDictionary[shape] = 1
            }
        }
        
        let highestValue = shapeDictionary.values.max()
        
        for (shape, count) in shapeDictionary {
            if shapeDictionary[shape] == highestValue {
                topShapes.append(shape)
//                print("Shape \(shape)")
            }
        }
//        print("Top shapes: \(topShapes)")
        return topShapes
    }
    
   
    
    func setupShape() {
        
        var shape = getMostCommonShape(array: self.selectedShapeId)
        
        if shape.count > 0 {
         let shape = shape[0]
        let jsonUrlString = "https://data.foli.fi/gtfs/v0/shapes/" + shape
//        print(jsonUrlString)
        guard let url = URL(string: jsonUrlString) else { print("url error"); return }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            //check err
            //check response status 299 ok
            
            guard let data = data else { print("data error"); return }
            
            do {
                
                var shape = try JSONDecoder().decode(latlong.self, from: data)
//                print("about to start")
                
                func makeLine() {
                    self.selectedBusLineLat.removeAll()
                    self.selectedBusLineLong.removeAll()
                    for data in shape {
                        self.selectedBusLineLat.append(data.lat)
                        self.selectedBusLineLong.append(data.lon)
                    }
                }
                
//                if self.selectedShapeId != self.lastSelectedShapeID.first {
                    makeLine()
//                } else {
//                    print("error sorry line 326")
//                }
                
                self.ShowLine()
                self.showAnnotations()
                
                
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
            
        }.resume()
        } else {
            func errorAlert() {
                let ac = UIAlertController(title: "Error", message: "Sorry, this route is no longer used", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Continue", style: .default))
                present(ac, animated: true)
            }

            DispatchQueue.main.async { self.MapView.removeOverlays(self.MapView.overlays)
                errorAlert();
                self.MapView.removeAnnotations(self.MapView.annotations)
            }
            
        }
}

    
    
    
    func ShowLine() {
        
        var location = [CLLocationCoordinate2D]()
        
        location.removeAll()
        DispatchQueue.main.async {
            self.MapView.removeOverlays(self.MapView.overlays)
        }
        for (long, lat) in zip(selectedBusLineLong, selectedBusLineLat) {
            
            
            location.append(CLLocationCoordinate2D(latitude: lat, longitude: long))
            
        }
        
         polyLine = MKPolyline(coordinates: location, count: location.count)
        
        
        
//        print(location)
    }
    
    func showAnnotations() {
//        print("started")
        
        DispatchQueue.main.async {
            self.MapView.removeAnnotations(self.MapView.annotations)
        }
//        print(selectedBusLat)
        for (long, lat) in zip(selectedBusLong, selectedBusLat) {
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            DispatchQueue.main.async { self.MapView.addAnnotation(annotation) }

           
        }
        
        var regionRect = polyLine.boundingMapRect
        
        
        let wPadding = regionRect.size.width * 0.25
        let hPadding = regionRect.size.height * 0.25
        
        //Add padding to the region
        regionRect.size.width += wPadding
        regionRect.size.height += hPadding
        
        //Center the region on the line
        regionRect.origin.x -= wPadding / 2
        regionRect.origin.y -= hPadding / 2
        
        DispatchQueue.main.async { self.MapView.setRegion(MKCoordinateRegion(regionRect), animated: true) }
        DispatchQueue.main.async { self.MapView.addOverlay(self.polyLine) }

        
    }
    
    
 
    
    func setMapView() {
        MapView.mapType                     = MKMapType.standard
        MapView.isZoomEnabled               = true
        MapView.isScrollEnabled             = true
        
        self.MapView.layer.cornerRadius     = 15.0
        self.MapView.clipsToBounds          = true
        
        view.addSubview(MapView)
    }
    
    func setBusLabel() {
        BusLabel.adjustsFontSizeToFitWidth = true
    }
    
    // Map stuff
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
//    func centerViewOnUserLocation() {
//        if let location = locationManager.location?.coordinate {
//            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
//            MapView.setRegion(region, animated: true)
//        }
//    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know that they have to turn on the setting
        }
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            MapView.showsUserLocation = false
//            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            
        case .denied:
            
            if askt == 0 {
                let ac = UIAlertController(title: "Denied", message: "If you still want to see you location on the map, you have to go to settings...", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Okay", style: .default))
                present(ac, animated: true)
                
                askt += 1
            }
            
            // Visa bussen ändå
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            if askt == 0 {
                let ac = UIAlertController(title: "Restricted", message: "It's possible that your parents/guardians have stopt your iPhone from showing your location, if you want to see your location on the map ask your parents/guardians for help.", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "Okay", style: .default))
                present(ac, animated: true)
                
                askt += 1
            }
            
        case .authorizedAlways:
            break
        }
    }
    
    func createBusPicker() {
        busPicker.delegate = self
        
        BusLabel.inputView = busPicker
    }
    
    func createToolBar() {
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(ViewController.dismissKeyboard))
        let resetButton = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(ViewController.resetPicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([resetButton, spacer, doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        BusLabel.inputAccessoryView = toolBar
        
        // Customizations
        
        
    }
    
    @objc func resetPicker() {
        BusLabel.text = ""
        busPicker.selectRow(0, inComponent: 0, animated: true)
        
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        setUpBusAPI()
        
        
    }
    
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: center, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        MapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
}

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return busNames.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return busNames[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedRoute = busNames[row]
        BusLabel.text = selectedRoute
        
    }
    
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        
        if annotation is MKUserLocation{
            return nil;
        }else{
            let pinIdent = "Pin";
            var pinView: MKPinAnnotationView;
        if let dequeuedView =       mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as?     MKPinAnnotationView {
            dequeuedView.annotation = annotation;
            pinView = dequeuedView;
        }else{
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdent);
        
            }
            return pinView;
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if(overlay is MKPolyline) {
            let polylineRender = MKPolylineRenderer(overlay: overlay)
            polylineRender.strokeColor = UIColor.red
            polylineRender.lineWidth = 5
            
            return polylineRender
        }
        return MKPolylineRenderer()
    }
}

